FROM gcr.io/distroless/static
LABEL maintainer="Kirill Tikhomirov aka worldbug"
USER nonroot:nonroot
COPY --chown=nonroot:nonroot ./app /app
ENTRYPOINT ["/app"]
