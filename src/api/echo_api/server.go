package echo_api

import (
	"net/http"
	"os"

	db "trueconf_task/src/database"
	model "trueconf_task/src/model"

	"github.com/labstack/echo"
)

type Echo_API_Impl struct {
	srv *echo.Echo
	db  db.DBHandler
	// тут течь будет
	reqs map[string]bool
}

func (api *Echo_API_Impl) Close() error {
	api.db.Close()
	return nil
}

func (api *Echo_API_Impl) Init() error {
	api.reqs = make(map[string]bool)

	api.db = db.GetDBHandler(os.Getenv("DB_HANDELR"))
	api.db.Init()

	api.srv = echo.New()
	api.srv.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Plas")
	})

	api.srv.POST("/users", api.AddUser)
	api.srv.GET("/users", api.GetUsersList)
	api.srv.GET("/users/:id", api.GetUserPerID)
	api.srv.PUT("/users/:id", api.EditUserPerID)
	api.srv.DELETE("/users/:id", api.DeleteUserPerID)

	api.srv.Logger.Fatal(api.srv.Start(":" + os.Getenv("APP_PORT")))
	return nil
}

func (api *Echo_API_Impl) AddUser(c echo.Context) error {
	u := new(model.UserRequest)
	if err := c.Bind(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if _, ok := api.reqs[u.RequsetID]; ok {
		return c.String(http.StatusAccepted, "OK")
	}
	api.reqs[u.RequsetID] = true
	if err := api.db.AddUser(u.User); err != nil {
		return echo.NewHTTPError(http.StatusConflict, err.Error())
	}
	return c.String(http.StatusCreated, "OK")
}

func (api *Echo_API_Impl) GetUsersList(c echo.Context) error {
	u, err := api.db.GetUsersList()
	if err != nil {
		return echo.NewHTTPError(http.StatusConflict, err.Error())
	}
	return c.JSON(http.StatusOK, u)
}

func (api *Echo_API_Impl) GetUserPerID(c echo.Context) error {
	usr, err := api.db.GetUserPerID(c.Param("id"))
	if err != nil {
		return echo.NewHTTPError(http.StatusConflict, err.Error())
	}
	return c.JSON(http.StatusOK, usr)
}

func (api *Echo_API_Impl) EditUserPerID(c echo.Context) error {
	u := new(model.UserRequest)
	if err := c.Bind(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if _, ok := api.reqs[u.RequsetID]; ok {
		return c.String(http.StatusAccepted, "OK")
	}
	api.reqs[u.RequsetID] = true
	if err := api.db.EditUserPerID(c.Param("id"), u.User); err != nil {
		return echo.NewHTTPError(http.StatusConflict, err.Error())
	}
	return c.String(http.StatusCreated, "OK")
}

func (api *Echo_API_Impl) DeleteUserPerID(c echo.Context) error {
	u := new(model.UserRequest)
	if err := c.Bind(u); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	err := api.db.DeleteUserPerID(c.Param("id"))
	if err != nil {
		return echo.NewHTTPError(http.StatusConflict, err.Error())
	}
	return c.String(http.StatusOK, "Deleted")
}
