package api_interface

import (
	echo_api "trueconf_task/src/api/echo_api"

	"github.com/labstack/echo"
)

// APIHandler - Интрерфейс API обработчика
type APIHandler interface {
	Init() error
	Close() error
	AddUser(echo.Context) error
	GetUsersList(echo.Context) error
	GetUserPerID(echo.Context) error
	EditUserPerID(echo.Context) error
	DeleteUserPerID(echo.Context) error
}

// GetDBHandler needed for select db driver
func GetAPIHandler(handlerName string) APIHandler {
	switch handlerName {
	case "echo":
		return &echo_api.Echo_API_Impl{}
	default:
		return &echo_api.Echo_API_Impl{}
	}
}
