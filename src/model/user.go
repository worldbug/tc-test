package model

// User - Модель пользователя
type User struct {
	ID            string `json:"id,omitempty"`
	NameDisplayed string `json:"name_displayed,omitempty"`
}

// UserRequest - Жсон запроса юезра
type UserRequest struct {
	RequsetID string `json:"requset_id,omitempty"`
	User      User   `json:"user,omitempty"`
}
