package json_db

import (
	"testing"
	model "trueconf_task/src/model"

	"gotest.tools/v3/assert"
)

func TestAddUser(t *testing.T) {
	cases := []struct {
		name    string
		input   model.User
		dbState JsonDBModel
	}{
		{
			"New user",
			model.User{
				ID:            "001",
				NameDisplayed: "Kolya",
			},
			JsonDBModel{
				Users: map[string]model.User{"0": {ID: "001", NameDisplayed: "Kolya"}},
			},
		},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			db := &JsonDB{}
			db.Init()

			db.AddUser(testCase.input)
			usr, _ := db.GetUserPerID(testCase.input.ID)
			assert.Equal(t, usr,
				testCase.dbState.Users[testCase.input.ID])
		})
	}
}
