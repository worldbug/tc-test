package json_db

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"strconv"
	"time"
	model "trueconf_task/src/model"

	"github.com/sirupsen/logrus"
)

var log = logrus.New()

type MetaInfo struct {
	LastUpdateTime int64 `json:"last_update_time,omitempty"`
	CreationTime   int64 `json:"creation_time,omitempty"`
}

// JsonDBModel Модель для храенения БД в памяти
type JsonDBModel struct {
	MetaInfo MetaInfo `json:"meta_info,omitempty"`

	// map[USER_ID] USER_STRUCT
	Users map[string]model.User `json:"users,omitempty"`
}

type JsonDB struct {
	filePath      string
	db            *JsonDBModel
	dbDumpCounter struct {
		current int
		limit   int
	}
}

func (jdb *JsonDB) Close() error {
	jdb.commit()
	return nil
}

func (jdb *JsonDB) Init() error {
	// Для снижения IO
	writeLim, _ := strconv.Atoi(os.Getenv("JSONDB_DUMP_FREQ"))
	if writeLim < 0 {
		writeLim = 3 // Стандратный рейт лим на запись
	}
	jdb.dbDumpCounter.limit = writeLim
	jdb.filePath = os.Getenv("JSONDB_DB_PATH")
	jdb.load()
	return nil
}
func (jdb *JsonDB) AddUser(usr model.User) error {
	if _, ok := jdb.db.Users[usr.ID]; ok {
		return errors.New("User already exist")
	}
	jdb.db.Users[usr.ID] = usr
	jdb.commit()
	return nil
}

func (jdb *JsonDB) GetUsersList() ([]model.User, error) {
	var usr []model.User
	for _, v := range jdb.db.Users {
		usr = append(usr, v)
	}
	jdb.commit()
	return usr, nil
}

func (jdb *JsonDB) GetUserPerID(id string) (model.User, error) {
	return jdb.db.Users[id], nil
}

func (jdb *JsonDB) EditUserPerID(id string, usr model.User) error {
	if _, ok := jdb.db.Users[id]; !ok {
		return errors.New("User not exist")
	}
	jdb.db.Users[id] = usr
	jdb.commit()
	return nil
}

func (jdb *JsonDB) DeleteUserPerID(id string) error {
	log.Println(id)
	if _, ok := jdb.db.Users[id]; !ok {
		return errors.New("User not exist")
	}
	delete(jdb.db.Users, id)
	jdb.commit()
	return nil
}

func (jdb *JsonDB) load() error {
	dbFile, err := os.OpenFile(jdb.filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	byteBuff, _ := ioutil.ReadAll(dbFile)
	defer dbFile.Close()
	json.Unmarshal(byteBuff, jdb.db)
	if jdb.db == nil {
		jdb.db = &JsonDBModel{}
		jdb.db.MetaInfo.CreationTime = time.Now().Unix()
		jdb.db.MetaInfo.LastUpdateTime = time.Now().Unix()
		jdb.db.Users = make(map[string]model.User)
	}
	return nil
}

func (jdb *JsonDB) commit() error {
	jdb.dbDumpCounter.current++
	if jdb.dbDumpCounter.current >= jdb.dbDumpCounter.limit {
		jdb.dbDumpCounter.current = 0
		jdb.db.MetaInfo.LastUpdateTime = time.Now().Unix()
		dbFile, _ := json.MarshalIndent(jdb.db, "", " ")
		_ = ioutil.WriteFile(jdb.filePath, dbFile, 0644)
	}
	log.Debugf("commit: %d %d", jdb.dbDumpCounter.current, jdb.dbDumpCounter.limit)
	return nil
}
