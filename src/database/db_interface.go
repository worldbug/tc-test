package db_interface

import (
	json_db "trueconf_task/src/database/json_db"
	model "trueconf_task/src/model"
)

type DBHandler interface {
	Init() error
	Close() error
	AddUser(model.User) error
	GetUsersList() ([]model.User, error)
	GetUserPerID(string) (model.User, error)
	EditUserPerID(string, model.User) error
	DeleteUserPerID(string) error
}

func GetDBHandler(handlerName string) DBHandler {
	switch handlerName {
	case "json":
		return &json_db.JsonDB{}
	default:
		return &json_db.JsonDB{}
	}
}
