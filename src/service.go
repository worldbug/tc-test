package main

import (
	"os"
	handler "trueconf_task/src/api"
)

func main() {
	api := handler.GetAPIHandler(os.Getenv("API_HANDLER"))
	api.Init()
	defer api.Close()
}
