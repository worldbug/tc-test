include .env

# Запусить сервер
run: 
	cd src && go run ./

gdlv:
	cd src && gdlv debug .  

test-add-user:
	curl -v  -H 'Content-Type: application/json' -X POST -d '{"requset_id":"01","user":{"id":"666","name_displayed":"Kolka"}}' http://0.0.0.0:$$APP_PORT/users

test-get-user-per-id:
	curl -v  http://0.0.0.0:$$APP_PORT/users/666

test-get-users:
	curl -v  http://0.0.0.0:$$APP_PORT/users

test-update-user:
	curl -v  -H 'Content-Type: application/json' -X PUT -d '{"requset_id":"02","user":{"id":"666","name_displayed":"Kolyanchik"}}' http://0.0.0.0:$$APP_PORT/users/666

test-get-delete-per-id:
	curl -v -X DELETE http://0.0.0.0:$$APP_PORT/users/666

build-app:
	cd src && CGO_ENABLED=0 go build \
		-installsuffix 'static' \
		-o ../app
build-container:
	podman build -t user-svc  . 
start-pod:
	podman pod start user-svc
stop-pod:
	podman pod stop user-svc

.PHONY: \
	run \
	clean-all
